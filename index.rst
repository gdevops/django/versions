.. index::
   pair: Django ; Versions
   ! Versions

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/django/versions/rss.xml>`_


.. _django_versions:

============================================
**Django Versions**
============================================

- https://github.com/django/django/releases
- https://www.djangoproject.com/download/#supported-versions
- https://github.com/django/django/graphs/contributors
- https://github.com/django/django
- https://groups.google.com/forum/#!forum/django-developers
- https://docs.djangoproject.com/en/dev/internals/release-process/#release-cadence
- https://docs.djangoproject.com/en/dev/internals/release-process/
- https://code.djangoproject.com/wiki
- https://github.com/django/deps
- https://upgradedjango.com/ (Maintenance is Mandatory)

:Release process: https://docs.djangoproject.com/en/dev/internals/release-process/#release-process

Django uses a time-based release schedule, with feature releases every
**eight months or so**.


:supported versions:  https://www.djangoproject.com/download/#supported-versions

.. figure:: images/supported_versions.png
   :align: center

   https://www.djangoproject.com/download/#supported-versions

.. toctree::
   :maxdepth: 6

   5.2/5.2
   5.1.6/5.1.6
   5.1/5.1
   5.0.6/5.0.6
   5.0.5/5.0.5
   5.0.4/5.0.4
   5.0.2/5.0.2
   5.0/5.0
   4.2/4.2
   4.1/4.1
   4.0/4.0
   3.2.7/3.2.7
   3.2.3/3.2.3
   3.2/3.2
   3.1/3.1
   3.0/3.0
   2.2/2.2
   2.0/2.0
