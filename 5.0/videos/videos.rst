.. index::
   pair: Sarah Boyce ; 5.0 (2023-12-04)

.. _sarah_boyce_2023_12_04:

==========================================================
**What's new in Django 5.0 !** by Sarah Boyce
==========================================================

- https://www.youtube.com/watch?v=lPl5Q5gv9G8
- Release notes: https://docs.djangoproject.com/en/dev/releases/5.0/
- Blog article on favourite features of 5.0 by Mariusz Felisiak: https://fly.io/django-beats/new-goodies-in-django-50
- Blog article on generated columns for SQLite by Paulo Melchiorre: https://www.paulox.net/2023/11/07/database-generated-columns-part-1-django-and-sqlite/
- Blog article on generated columns for PostgreSQL by Paulo Melchiorre: https://www.paulox.net/2023/11/24/database-generated-columns-part-2-django-and-postgresql/
- Blog article on using the new form features by David Smith: https://smithdc.uk/blog/2023/bootstrap_form_in_vanilla_django/
- DjangoCon conference talk Good Form by David Smith: DjangoCon Europe 2023 : https://www.youtube.com/watch?v=tEDMHWDMMUI&ab_channel=DjangoConEurope 


..  youtube:: lPl5Q5gv9G8

Django 5.0 is out! Let's unwrap the new features of our favourite Python  web framework together 🎁
=========================================================================================================


Have you been counting down the days? Not to Christmas... to the new 
Django release! 

Django 5.0 is out! Let's unwrap the new features of our favourite Python 
web framework together 🎁

What you'll see in this video
=================================

- `00:00 Intro <https://www.youtube.com/watch?v=lPl5Q5gv9G8>`_
- `00:10 New feature: db_default <https://www.youtube.com/watch?v=lPl5Q5gv9G8&t=10s>`_
- `00:59 New feature: GeneratedField <https://www.youtube.com/watch?v=lPl5Q5gv9G8&t=59s>`_
- `02:16 Other ORM new features <https://www.youtube.com/watch?v=lPl5Q5gv9G8&t=136s>`_
- `02:47 New feature: as_field_group <https://www.youtube.com/watch?v=lPl5Q5gv9G8&t=167s>`_
- `04:16 Community update <https://www.youtube.com/watch?v=lPl5Q5gv9G8&t=256s>`_

Resources
==============

- Release notes: https://docs.djangoproject.com/en/dev/releases/5.0/
- Blog article on favourite features of 5.0 by Mariusz Felisiak: https://fly.io/django-beats/new-goodies-in-django-50
- Blog article on generated columns for SQLite by Paulo Melchiorre: https://www.paulox.net/2023/11/07/database-generated-columns-part-1-django-and-sqlite/
- Blog article on generated columns for PostgreSQL by Paulo Melchiorre: https://www.paulox.net/2023/11/24/database-generated-columns-part-2-django-and-postgresql/
- Blog article on using the new form features by David Smith: https://smithdc.uk/blog/2023/bootstrap_form_in_vanilla_django/
- DjangoCon conference talk Good Form by David Smith: DjangoCon Europe 2023 : https://www.youtube.com/watch?v=tEDMHWDMMUI&ab_channel=DjangoConEurope 

GitHub profiles of community members featured - give them a follow if you want to see what they do next !
===========================================================================================================

- Lily Foote: https://github.com/LilyFoote
- Paulo Melchiorre: https://github.com/pauloxnet
- David Smith: https://github.com/smithdc1
- Natalia Bidart: https://github.com/nessita
- Mariusz Felisiak: https://github.com/felixxm
- Carlton Gibson: https://github.com/carltongibson

I want to thank our Django Fellows and our community for delivering such 
a fantastic release 👏

There are many unsung heroes but here are the 204 contributors to 
Django 5.0: https://gist.github.com/felixxm/89e253a67e364c72f8d4ec5a04818a92

Like what you see and want to see more? You can support the Django community 
by making a donation to the Django Software Foundation: https://www.djangoproject.com/fundraising/

I'm also new here 👋 thank you for reading this far. Please like, comment 
or subscribe if you want to see more content of Django ❤️
