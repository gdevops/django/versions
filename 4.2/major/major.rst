
.. _django_4_2_major_features:

==========================================================
**Django 4.2 major features**
==========================================================


pyscopg3 support
====================

- https://docs.djangoproject.com/en/4.2/releases/4.2/#psycopg-3-support
- `pyscopg3 support <https://docs.djangoproject.com/en/dev/releases/4.2/#psycopg-3-support>`_
- https://www.psycopg.org/psycopg3/docs/basic/from_pg2.html#what-s-new-in-psycopg-3

Django now supports psycopg version 3.1.8 or higher.

To update your code, install the psycopg library, you don’t need to
change the ENGINE as django.db.backends.postgresql supports both libraries.

Support for psycopg2 is likely to be deprecated and removed at some point
in the future.


Example with poetry

::

     ❯ poetry add psycopg

::

    Using version ^3.1.8 for psycopg

    Updating dependencies
    Resolving dependencies... (0.5s)

    Writing lock file

    Package operations: 1 install, 0 updates, 0 removals

      • Installing psycopg (3.1.8)

::


    ✦ ❯ poetry remove psycopg2-binary

::

    Updating dependencies
    Resolving dependencies... (0.4s)

    Writing lock file

    Package operations: 0 installs, 0 updates, 1 removal

      • Removing psycopg2-binary (2.9.6)


What’s new in Psycopg 3
----------------------------

- https://www.psycopg.org/psycopg3/docs/basic/from_pg2.html#what-s-new-in-psycopg-3


Comments on columns and tables
================================

- https://docs.djangoproject.com/en/4.2/releases/4.2/#comments-on-columns-and-tables

The new Field.db_comment and Meta.db_table_comment options allow creating
comments on columns and tables, respectively.


.. _custom_file_storages:

Custom file storages (STORAGES)
====================================

- https://docs.djangoproject.com/en/4.2/releases/4.2/#custom-file-storages
- https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-STORAGES

The new **STORAGES** setting allows configuring multiple custom file storage
backends.

It also controls storage engines for managing files (the "default" key)
and static files (the "staticfiles" key).

The old DEFAULT_FILE_STORAGE and STATICFILES_STORAGE settings are
deprecated as of this release.

STORAGES

New in Django 4.2::

    Default:

    STORAGES = {
        "default": {
            "BACKEND": "django.core.files.storage.FileSystemStorage",
        },
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
    }


Upgrade with django-upgrade
--------------------------------

::

    django-upgrade --target-version 4.2 config/settings/base.py

::

    STORAGES = {
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
    }

.. warning:: do not forget

::

    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },


Support for asynchronous streaming responses
==============================================

- https://docs.djangoproject.com/en/4.2/releases/4.2/#requests-and-responses


Support for async interfaces for related managers and Mode
=============================================================

- https://docs.djangoproject.com/en/4.2/releases/4.2/#models


Tutorial on django-debug-toolbar
=====================================

- https://docs.djangoproject.com/en/4.2/intro/tutorial08/
