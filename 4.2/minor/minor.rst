
.. _django_4_2_minor_features:

==========================================================
Django 4.2 minor features
==========================================================

- https://docs.djangoproject.com/en/dev/releases/4.2/#minor-features

Django.contrib.admin
========================

- The light or dark color theme of the admin can now be toggled in the UI,
  as well as being set to follow the system setting.
- The admin’s font stack now prefers system UI fonts and no longer requires
  downloading fonts.
  Additionally, CSS variables are available to more easily override the
  default font families.
- The admin/delete_confirmation.html template now has some additional
  blocks and scripting hooks to ease customization.
- The chosen options of filter_horizontal and filter_vertical widgets
  are now filterable.
- The admin/base.html template now has a new block nav-breadcrumbs which
  contains the navigation landmark and the breadcrumbs block.
- ModelAdmin.list_editable now uses atomic transactions when making edits.
- jQuery is upgraded from version 3.6.0 to 3.6.4.



Django.contrib.gis
==================

- The GeoJSON serializer now outputs the id key for serialized features,
  which defaults to the primary key of objects.
- The GDALRaster class now supports pathlib.Path.
- The GeoIP2 class now supports .mmdb files downloaded from DB-IP.
- The OpenLayers template widget no longer includes inline CSS
  (which also removes the former map_css block) to better comply with a
  strict Content Security Policy.
- OpenLayersWidget is now based on OpenLayers 7.2.2 (previously 4.6.5).
- The new isempty lookup and IsEmpty() expression allow filtering empty
  geometries on PostGIS.
- The new FromWKB() and FromWKT() functions allow creating geometries
  from Well-known binary (WKB) and Well-known text (WKT) representations.


django.contrib.postgres
===========================

- The new trigram_strict_word_similar lookup, and the TrigramStrictWordSimilarity()
  and TrigramStrictWordDistance() expressions allow using trigram strict word similarity.
- The arrayfield.overlap lookup now supports QuerySet.values()
  and values_list() as a right-hand side


Database backends
======================

- The new "assume_role" option is now supported in OPTIONS on PostgreSQL
  to allow specifying the session role.
- The new "server_side_binding" option is now supported in OPTIONS on
  PostgreSQL with psycopg 3.1.8+ to allow using server-side binding cursors.


Internationalization
===========================

- Added support and translations for the Central Kurdish (Sorani) language.
- The LocaleMiddleware now respects a language from the request when
  i18n_patterns() is used with the prefix_default_language argument set to False.

Management Commands
============================

- makemessages command now supports locales with private sub-tags such
  as nl_NL-x-informal.
- The new makemigrations --update option merges model changes into the latest migration and optimizes the resulting operations



Models
=============

- QuerySet now extensively supports filtering against Window functions
  with the exception of disjunctive filter lookups against window functions
  when performing aggregation.
- prefetch_related() now supports Prefetch objects with sliced querysets.
- Registering lookups on Field instances is now supported.
- The new robust argument for on_commit() allows performing actions that
  can fail after a database transaction is successfully committed.
- The new KT() expression represents the text value of a key, index,
  or path transform of JSONField.
- Now now supports microsecond precision on MySQL and millisecond precision
  on SQLite.
- F() expressions that output BooleanField can now be negated using ~F()
  (inversion operator).
- Model now provides asynchronous versions of some methods that use
  the database, using an a prefix: adelete(), arefresh_from_db(), and asave().
- Related managers now provide **asynchronous versions of methods** that
  change a set of related objects, using an **a prefix**: aadd(), aclear(),
  aremove(), and aset().
- CharField.max_length is no longer required to be set on PostgreSQL,
  which supports unlimited VARCHAR columns


Requests and Responses
===========================

StreamingHttpResponse now supports async iterators when Django is served via ASGI


Tests
========

The test --debug-sql option now formats SQL queries with sqlparse.

The RequestFactory, AsyncRequestFactory, Client, and AsyncClient classes now support the headers parameter, which accepts a dictionary of header names and values. This allows a more natural syntax for declaring headers.

.. code-block:: python

    # Before:
    self.client.get("/home/", HTTP_ACCEPT_LANGUAGE="fr")
    await self.async_client.get("/home/", ACCEPT_LANGUAGE="fr")

    # After:
    self.client.get("/home/", headers={"accept-language": "fr"})
    await self.async_client.get("/home/", headers={"accept-language": "fr"})


django-debug-toolbar 4.0 was just released with compatibility with psycopg3
================================================================================

- https://masto.ai/@danjac/110136054909508550

django-debug-toolbar 4.0 was just released with compatibility with psycopg3

Thanks to the developers for getting this out quickly! Debug toolbar
working nicely again with django 4.2.
