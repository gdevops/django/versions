.. index::
   pair: Django ; 5.0.6 (2024-05-07)

.. _django_5_0_6:

==========================================================
**Django 5.0.6** (2024-05-07)
==========================================================

- https://docs.djangoproject.com/en/5.0/releases/5.0.6/


Django 5.0.6 fixes a packaging error in 5.0.5

