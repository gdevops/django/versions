.. index::
   pair: Django 3.1 ; Async view

.. _django_3_1_async_view:

=================================================================
Django 3.1 **async view** is done (=> async phase 2 is done !)
=================================================================

.. seealso::

   - :ref:`dep_0009`
   - https://code.djangoproject.com/wiki/AsyncProject
   - https://www.aeracode.org/2018/06/04/django-async-roadmap/
   - https://groups.google.com/forum/#!msg/django-developers/5CVsR9FSqmg/UiswdhLECAAJ
   - https://forum.djangoproject.com/c/internals/async/8
   - https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django
   - https://docs.djangoproject.com/en/3.1/topics/async/
   - https://github.com/django/django/commit/fc0fa72ff4cdbf5861a366e31cb8bbacd44da22d
   - :ref:`django_async_views_prog`
   - https://docs.djangoproject.com/en/dev/releases/3.1/#asynchronous-views-and-middleware-support
   - https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=42
   - https://github.com/django/asgiref/
   - :ref:`structured_concurrency`
   - :ref:`asyncio`
   - :ref:`anyio`
   - :ref:`sniffio`
   - :ref:`python_asgi`
   - :ref:`uvicorn`


.. contents::
   :depth: 5


History
==========

Phase 1 : Django 3.0
----------------------

.. seealso::

   - :ref:`django_3_0_asgi`

Phase 2 : Django 3.1
----------------------

.. figure:: phase_2.png
   :align: center

   https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=61
   https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=1

Django 3.1 announce
====================

.. seealso::

   - https://docs.djangoproject.com/en/dev/releases/3.1/#asynchronous-views-and-middleware-support
   - :ref:`asyncio`


To get started with **async views**, you need to declare a view using **async def**:


Example 1
----------

.. code-block:: python

    # https://docs.python.org/3/library/asyncio.html
    # https://docs.python.org/3/library/asyncio-api-index.html
    import asyncio

    async def my_view(request):
        await asyncio.sleep(0.5)
        return HttpResponse('Hello, async world!')


.. _example_2_async_django:

Example 2
----------

models.py
+++++++++++

.. code-block:: python
   :linenos:

    # https://docs.python.org/3/library/asyncio-task.html
    import asyncio
    # https://docs.djangoproject.com/en/3.1/topics/async/#async-to-sync
    from asgiref.sync import sync_to_async
    def sync_update_conges():
        print("Begin sync_update_conges()")
        PROJETS_CONGES = Projet.objects.filter(designation__icontains="abs")
        PROJETS_CONGES_ID = [p.id for p in PROJETS_CONGES]


    loop = asyncio.get_event_loop()
    async_function = sync_to_async(sync_update_conges)
    loop.create_task(async_function())



.. _example_3_async_django:

Example 3 async Django
------------------------

urls.py
+++++++++++++++

.. code-block:: python
   :linenos:


    path("asyncview/test1", views_asgi.async_view_test_1, name="asgi_async_view_test1"),
    path("syncview/test1", views_asgi.sync_view_test_1, name="asgi_sync_view_test1"),
    path(
        "asyncview/smoke_some_meats",
        views_asgi.smoke_some_meats,
        name="asgi_async_smoke_some_meats",
    ),
    path(
        "asyncview/async_hello", views_asgi.async_hello, name="asgi_async_async_hello"
    ),

views_asgi.py
+++++++++++++++


.. code-block:: python
   :linenos:

    # https://docs.python.org/3/library/asyncio-task.html
    import asyncio
    # https://docs.djangoproject.com/en/3.1/topics/async/#async-to-sync
    from asgiref.sync import sync_to_async

    def orm_get_fiches_temps_en_calcul() -> None:
        """Incrément du temps passé pour les FICHE_EN_CALCUL."""
        now = datetime.now()
        logger.info(
            f"Begin task_update_fiches_temps_en_calcul() {now.hour:02}H{now.minute:02}mn{now.second:02}s"
        )
        liste_fiches_en_calcul = None
        try:
            liste_fiches_en_calcul = FicheTemps.objects.filter(
                etat=FICHE_STATUS["FICHE_EN_CALCUL"]
            )
            if liste_fiches_en_calcul.count() == 0:
                now = datetime.now()
                logger.info(
                    f"pas de fiches en calcul {now.hour:02}H{now.minute:02}mn{now.second:02}s"
                )
            else:
                # parcours des fiches de temps en calcul
                for fiches_temps in liste_fiches_en_calcul:
                    try:
                        fiches_temps.temps_impute = fiches_temps.temps_impute + timedelta(
                            seconds=INCREMENT_TIME_SECONDS
                        )
                        logger.info(f"Temps impute:{fiches_temps.temps_impute}")
                        fiches_temps.save()
                    except:
                        logger.error("task_update_fiches_temps_en_calcul()")

        except Exception as error:
            logger.error(f"Pb in task_update_fiches_temps_en_calcul = {error=}")

        return liste_fiches_en_calcul


    async def _async_hello():
        while True:
            loop = asyncio.get_event_loop()
            async_function = sync_to_async(orm_get_fiches_temps_en_calcul)
            loop.create_task(async_function())
            await asyncio.sleep(5)


    async def async_hello(request: HttpRequest) -> HttpResponse:
        """Tâche périodique asynchrone.

        http://localhost:8004/fiches_temps/asyncview/async_hello
        """
        loop = asyncio.get_event_loop()
        loop.create_task(_async_hello())
        return HttpResponse(f"Non blocking hello")


**All asynchronous features are supported whether you are running under
WSGI or ASGI mode**.

**However, there will be performance penalties using async code in WSGI mode**.

You can read more about the specifics in `Asynchronous support documentation <https://docs.djangoproject.com/en/dev/topics/async/>`_.

You are free to mix async and sync views, middleware, and tests as much
as you want.
Django will ensure that you always end up with the right execution context.

We expect most projects will keep the majority of their views synchronous,
and **only have a select few running in async mode but it is entirely your choice**.

Django’s ORM, cache layer, and other pieces of code that do long-running
network calls :ref:`do not yet support async access <django_3_2>`.

We expect to add support for them :ref:`in upcoming releases <django_3_2>`.

**Async views** are ideal, however, if you are doing a lot of API or HTTP
calls inside your view, you can now natively do all those HTTP calls
in parallel to considerably speed up your view’s execution.

Asynchronous support should be entirely backwards-compatible and we have
tried to ensure that it has no speed regressions for your existing,
synchronous code.

It should have no noticeable effect on any existing Django projects.

Mariusz Felisiak
===================

Many thanks to @andrewgodwin for adding support for async views,
middleware, and tests to #Django #async

.. figure:: async_views.png
   :align: center

   https://x.com/MariuszFelisiak/status/1240357596315467778?s=20


Andrew Godwin
==============

.. seealso::

   - :ref:`django_dep_0009`
   - https://code.djangoproject.com/wiki/AsyncProject
   - https://www.aeracode.org/2018/06/04/django-async-roadmap/
   - https://github.com/django/django/commit/fc0fa72ff4cdbf5861a366e31cb8bbacd44da22d
   - https://arunrocks.com/a-guide-to-asgi-in-django-30-and-its-performance/
   - https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=1
   - https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=42


**It is done** - Django 3.1 will have **async views !**
Thanks to everyone who's helped out with this.

.. figure:: async_views_godwin.png
   :align: center

   https://x.com/andrewgodwin/status/1240357729174052866?s=20


The fc0fa72ff4cdb git commit
------------------------------

.. seealso::

   - https://github.com/django/django/commit/fc0fa72ff4cdbf5861a366e31cb8bbacd44da22d


Simon Willison
=================

.. seealso::

   - https://simonwillison.net/2009/Nov/23/node/
   - https://x.com/simonw/status/1287146436837044224?s=20
   - https://x.com/simonw/status/1290665393833381888?s=20

.. figure:: ../simon_willison.png
   :align: center

   https://x.com/simonw/status/1287146436837044224?s=20

::

    Node is a tempting option for anything involving comet, file uploads or
    even just mashing together potentially slow loading web APIs.

    10.5 years later, I'm excited about async views in Django 3.1 for exactly
    the same reasons (except we don't call live updates "comet" any more)


Tutorials
===========

.. toctree::
   :maxdepth: 3

   django/django
   jace/jace
   jochen/jochen
   arun/arun
   sanket/sanket
