
.. _django_3_1_async_view_jace:

===================================================================
Async Views in Django 3.1 by **Jace Medlin**, August 17th, 2020
===================================================================

.. seealso::

   - https://testdriven.io/blog/django-async-views/
   - https://github.com/based-jace



Introduction
===============

Writing asynchronous code gives you the ability to speed up your
application with **little effort**.

With Django 3.1 finally supporting async views, middleware, and tests,
**now's a great time to get them under your belt**.

This post looks at how to get started with **Django's new asynchronous views**.

Objectives (..., Simplify basic background tasks with Django’s async views,...)
------------------------------------------------------------------------------------

By the end of this post, you should be able to:

- Write an asynchronous view in Django
- Make a non-blocking HTTP request in a Django view
- **Simplify basic background tasks with Django's async views**
- Use sync_to_async to make a synchronous call inside an async view
- Explain when you should and shouldn't use async views

You should also be able to answer the following questions:

- What if you make a synchronous call inside an async view ?
- What if you make a synchronous and an asynchronous call inside an async view ?
- Is Celery still necessary with Django's async views ?

Prerequisites
==============

As long as you're already familiar with Django itself, adding asynchronous
functionality to non-class-based views is extremely straightforward.

Dependencies
---------------

- Python >= 3.8
- Django >= 3.1
- Uvicorn
- HTTPX

What is ASGI ?
================

**ASGI** stands for **Asynchronous Server Gateway Interface**.

It's the modern, asynchronous follow-up to WSGI, providing a standard
for creating asynchronous Python-based web apps.

Another thing worth mentioning is that ASGI is backwards-compatible with
WSGI, making it a good excuse to switch from a WSGI server like Gunicorn
or uWSGI to an ASGI server like **Hypercorn**, Uvicorn or Daphne **even if
you're not ready to switch to writing asynchronous apps**.

Creating the App
==================

Create a new project directory along with a new Django project::

    $ mkdir django-async-views && cd django-async-views
    $ python3.8 -m venv env
    $ source env/bin/activate

    (env)$ pip install django
    (env)$ django-admin.py startproject hello_async .

Feel free to swap out virtualenv and Pip for Poetry or Pipenv.

Django will run your async views if you're using the built-in development
server, **but it won't actually run them asynchronously**, so we'll use
Uvicorn to stand up the server.

Install it::

    (env)$ pip install uvicorn

To run your project with Uvicorn, you use the following command from
your project's root::

    uvicorn {name of your project}.asgi:application

In our case, this would be::

    (env)$ uvicorn hello_async.asgi:application

Next, let's create our first async view

Add a new file to hold your views in the "hello_async" folder, and then
add the following view:

.. code-block:: python
   :linenos:

    # hello_async/views.py

    from django.http import HttpResponse


    async def index(request):
        return HttpResponse("Hello, async Django!")

Creating async views in Django is as simple as creating a synchronous
view -- **all you need to do is add the async keyword**.

Update the URLs:

.. code-block:: python
   :linenos:

    # hello_async/urls.py

    from django.contrib import admin
    from django.urls import path

    from hello_async.views import index


    urlpatterns = [
        path("admin/", admin.site.urls),
        path("", index),
    ]

Now, in a terminal in your root folder, run::

    (env)$ uvicorn hello_async.asgi:application --reload

The --reload flag tells uvicorn to watch your files for changes and
reload if it finds any. That was probably self-explanatory.

Open http://localhost:8000/ in your favorite web browser::

    Hello, async Django!

Not the most exciting thing in the world, but, hey, it's a start.

It's worth noting that running this view with a Django's built-in
development server will result in exactly the same functionality
and output. This is because we're not actually doing anything asynchronous
in the handler.


HTTPX (A next generation HTTP client for Python)
===================================================

.. seealso::

   - https://github.com/encode/httpx

Introduction
--------------

It's worth noting that async support is **entirely backwards-compatible**,
so you can mix async and sync views, middleware, and tests.

Django will execute each in the proper execution context.

Install HTTPX
-------------------

::

    (env)$ pip install httpx


views.py
===========



To demonstrate this, add a few new views:

.. code-block:: python
   :linenos:

    import asyncio
    from time import sleep

    # https://github.com/encode/httpx
    import httpx
    from django.http import HttpResponse
    from django.http.request import HttpRequest


    # helpers
    async def http_call_async():
        print("http_call_async")
        try:
            for num in range(1, 6):
                await asyncio.sleep(1)
                print(f"async {num=}")

            async with httpx.AsyncClient() as client:
                r = await client.get("https://httpbin.org/")
                print(r)
        except Exception as error:
            print(f"{error}=")


    def http_call_sync():
        for num in range(1, 6):
            sleep(1)
            print(num)
        r = httpx.get("https://httpbin.org/")
        print(r)


    # views
    async def async_hello(request: HttpRequest) -> HttpResponse:
        return HttpResponse("Hello, async Django!")


    async def async_view_test_1(request: HttpRequest) -> HttpResponse:
        loop = asyncio.get_event_loop()
        loop.create_task(http_call_async())
        return HttpResponse("Non-blocking HTTP request")


    def sync_view_test_1(request: HttpRequest) -> HttpResponse:
        http_call_sync()
        return HttpResponse("Blocking HTTP request")


Update the URLs
===============

.. code-block:: python
   :linenos:

    # hello_async/urls.py

    from django.contrib import admin
    from django.urls import path

    from hello_async.views import async_view_test_1, sync_view_test_1


    urlpatterns = [
        path("admin/", admin.site.urls),
        path("async/", async_view_test_1),
        path("sync/", sync_view_test_1),
    ]



http://localhost:8000/async/
===============================

With the server running, navigate to http://localhost:8000/async/.
You should immediately see the response::

    Non-blocking HTTP request


In your terminal you should see::

    INFO:     127.0.0.1:60374 - "GET /async/ HTTP/1.1" 200 OK
    1
    2
    3
    4
    5
    <Response [200 OK]>

Here, the HTTP response is sent back before the first sleep call.


http://localhost:8000/sync/
===============================

Next, navigate to http://localhost:8000/sync/. It should take about
five seconds to get the response::

    Blocking HTTP request


Turn to the terminal::

    1
    2
    3
    4
    5
    <Response [200 OK]>
    INFO:     127.0.0.1:60375 - "GET /sync/ HTTP/1.1" 200 OK

Here, the HTTP response is sent after the loop and the request to
https://httpbin.org/ completes.


Smoking Some Meats
=====================

Now, let's write a view that **runs a simple task in the background**.

Back in your project's URLconf, create a new path at smoke_some_meats:


.. code-block:: python
   :linenos:

    # hello_async/urls.py

    from django.contrib import admin
    from django.urls import path

    from hello_async.views import async_view_test_1, sync_view_test_1, smoke_some_meats


    urlpatterns = [
        path("admin/", admin.site.urls),
        path("smoke_some_meats/", smoke_some_meats),
    ]


async def smoke
-----------------

Back in your views, create a **new async function called smoke**.

This function takes two parameters: a list of strings called smokables
and a string called flavor.

These will default to a list of smokable meats and "Sweet Baby Ray's",
respectively.

.. code-block:: python
   :linenos:

    # hello_async/views.py

    from typing import List

    async def smoke(smokables: List[str] = None, flavor: str = "Sweet Baby Ray's") -> None:
        """ Smokes some meats and applies the Sweet Baby Ray's """

        if smokables is None:
            smokables = [
                "ribs",
                "brisket",
                "lemon chicken",
                "salmon",
                "bison sirloin",
                "sausage",
            ]

        if (loved_smokable := smokables[0]) == "ribs":
            loved_smokable = "meats"

        for smokable in smokables:
            print(f"Smoking some {smokable}....")
            await asyncio.sleep(1)
            print(f"Applying the {flavor}....")
            await asyncio.sleep(1)
            print(f"{smokable.capitalize()} smoked.")

        print(f"Who doesn't love smoked {loved_smokable}?")

The first line of the function instantiates the default list of meats
if smokables isn't provided.

The second "if" statement then sets a variable called loved_smokable to
the first object in smokables, so long as the first object isn't "ribs."

The for loop asynchronously applies the flavor (read: Sweet Baby Ray's)
to the smokables (read: smoked meats).


async def smoke_some_meats
-----------------------------

Next, create the async view that uses **the async smoke function**:

.. code-block:: python
   :linenos:

    # hello_async/views.py

    async def smoke_some_meats(request: HttpRequest) -> HttpResponse:
        loop = asyncio.get_event_loop()
        smoke_args = []

        if to_smoke := request.GET.get("to_smoke"):
            # Grab smokables
            to_smoke = to_smoke.split(",")
            smoke_args += [[smokable.lower().strip() for smokable in to_smoke]]

            # Do some string prettification
            if (smoke_list_len := len(to_smoke)) == 2:
                to_smoke = " and ".join(to_smoke)
            elif smoke_list_len > 2:
                to_smoke[-1] = f"and {to_smoke[-1]}"
                to_smoke = ", ".join(to_smoke)

        else:
            to_smoke = "meats"

        if flavor := request.GET.get("flavor"):
            smoke_args.append(flavor)

        loop.create_task(smoke(*smoke_args))

        return HttpResponse(f"Smoking some {to_smoke}....")

This view takes the optional query params:

- **to_smoke** is a comma-delimited list of things to smoke,
- and flavor is what you're applying to them.


::

    http://localhost:8004/smoke_some_meats?flavor=blue
    http://localhost:8004/smoke_some_meats?to_smoke=banana,oranges&flavor=burned



The first thing this view does (which can't be done in a standard sync view)
is grab the event loop with asyncio.get_event_loop().

It then parses the query params, if applicable (and does some string
cleanup for the final print statement).

If we don't pass in anything to smoke, to_smoke defaults to "meats."

Finally, a response is returned to let the user know they're being
prepared a delicious BBQ meal.


Sync and Async Calls
======================

Q: What if you make a synchronous and an asynchronous call inside an
async view ?

.. warning:: Don't do this.

Synchronous and asynchronous views tend to work best for different purposes.

**If you have blocking functionality in an async view, at best it's going
to be no better than just using a synchronous view**.


Sync to Async
===============

If you need to make a synchronous call inside an async view (**like to
interact with the database via the Django ORM**, for example), use
**sync_to_async** either as a wrapper or a decorator.

Example:

.. code-block:: python
   :linenos:

    # hello_async/views.py

    async def async_with_sync_view(request):
        loop = asyncio.get_event_loop()
        async_function = sync_to_async(http_call_sync)
        loop.create_task(async_function())
        return HttpResponse("Non-blocking HTTP request (via sync_to_async)")

Add the import to the top::

    from asgiref.sync import sync_to_async


Add the URL
============

.. code-block:: python
   :linenos:

    # hello_async/urls.py

    from django.contrib import admin
    from django.urls import path

    from hello_async.views import (
        index,
        async_view,
        sync_view,
        smoke_some_meats,
        burn_some_meats,
        async_with_sync_view
    )


    urlpatterns = [
        path("admin/", admin.site.urls),
        path("smoke_some_meats/", smoke_some_meats),
        path("burn_some_meats/", burn_some_meats),
        path("sync_to_async/", async_with_sync_view),
        path("async/", async_view),
        path("sync/", sync_view),
        path("", index),
    ]

Test it out in your browser at http://localhost:8003/sync_to_async/.


In your terminal you should see::

    INFO:     127.0.0.1:61365 - "GET /sync_to_async/ HTTP/1.1" 200 OK
    1
    2
    3
    4
    5
    <Response [200 OK]>

Using sync_to_async, the blocking synchronous call was processed in a
background thread, allowing the HTTP response to be sent back before the
first sleep call.


.. _celery_and_async_views:

Celery and Async Views
===========================

Q: Is Celery still necessary with Django's async views ?

It depends.

**Django's async views offer similar functionality to a task or message queue
without the complexity**.

If you're using (or are considering) Django and want to do something simple
(such as send an email to a new subscriber or call an external API),
async views are a great way to accomplish this quickly and easily.

If you need to perform much-heavier, long-running background processes,
you'll still want to use Celery or RQ.

**It should be noted that to use async views effectively, you should only
have async calls in the view**.

Task queues, on the other hand, use workers on separate processes, and
are therefore capable of running synchronous calls in the background,
on multiple servers.

By the way, by no means must you choose between async views and a message
queue, you can easily use them in tandem.

For Example: You could use an async view to send an email or make a
one-off database modification, but have Celery clean out your database
at a scheduled time every night or generate and send customer reports.


Conclusion
============

In conclusion, although this was a simple use-case, it should give you a
rough idea of the possibilities that Django's new asynchronous views
open up.

Some other things to try in your async views are sending emails, calling
third-party APIs, and writing to a file.

Think of those views in your code that have simple processes in them
that don't necessarily need to return anything directly to the end user
those can quickly be converted to async.

For more on Django's newfound asynchronicity, see :ref:`this excellent post
that covers the same topic <django_31_async_jochen>` as well as multithreading
and testing.
