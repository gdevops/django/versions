.. index::
   pair: Django 3.1 ; Async view

.. _django_3_1_async_sanket:

==================================================================================
**Django Async: What's new and what's next ?**  By **Sanket** on August 14, 2020
==================================================================================

.. seealso::

   - https://deepsource.io/blog/django-async-support/
   - https://deepsource.io/for-python/

Starting with Django 3.1, the latest version that dropped a couple of
weeks ago, Django now supports fully asynchronous request path.

This is exciting for everyone who’s been waiting on the edge of their
seats ever since Andrew Godwin’s DEP 0009 was approved by Django Technical
Board in July 2019.

Read on to know all about what this release means if you have a Django
application in production and looking to add async support.

At DeepSource, we’re working on adding more Django issues in our `Python
analyzer <https://deepsource.io/for-python/>`_, which will also include
async-specific bug risks and anti-patterns.


Is ASGI required ?
----------------------

All of Django’s async features are fully supported on both WSGI and ASGI
although there will be performance penalties if you run async code with
WSGI and long-running requests won’t be efficient.

If you want to make your sync code work well with your all-new async
views and ASGI mode, use of the sync_to_async decorator is recommended.

Where to use async views ?
---------------------------

If you are doing a lot of external HTTP calls from a view, async views
allow you to natively make those calls in parallel.

This can provide great speed bumps, especially if you had been using
async code inside sync views before.

If your views involve heavy-lifting calculations or long-running network
calls to be done as part of the request path, it’s a great use case for
using async views.


Where you can’t use async in Django yet
---------------------------------------------

The ORM, cache layer, and several other parts of code that involve
long-running network calls **do not support async yet**.

**Support for async features in the ORM is expected to come sooner as it
is a part of the initial DEP**.

Features like templating and cache backends will need some more time, as
those will need their own separate DEPs and research to be fully async.
