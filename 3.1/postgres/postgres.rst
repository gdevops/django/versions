
.. _django_3_1_postgres:

==================================================================
Django 3.1 **postgres**
==================================================================

.. seealso::

   - https://docs.djangoproject.com/en/3.1/ref/contrib/postgres/search/
   - https://docs.djangoproject.com/en/3.1/topics/db/queries/
   - https://www.postgresql.org/docs/current/textsearch.html


.. toctree::
   :maxdepth: 3

   searchquery_websearch/searchquery_websearch
   searchheadline/searchheadline
