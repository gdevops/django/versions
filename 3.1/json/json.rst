.. index::
   pair: Django ; JSON

.. _django_3_1_json:

============================================================
Django 3.1  JSONField for all supported database backends
============================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/releases/3.1/#jsonfield-for-all-supported-database-backends


Django now includes models.JSONField and forms.JSONField that can be
used on all supported database backends.

Both fields support the use of custom JSON encoders and decoders.

The model field supports the introspection, lookups, and transforms
that were previously PostgreSQL-only.

If your project uses django.contrib.postgres.fields.JSONField, plus the
related form field and transforms, you should adjust to use the new fields,
and generate and apply a database migration.

For now, the old fields and transforms are left as a reference to the
new ones and are deprecated as of this release.
