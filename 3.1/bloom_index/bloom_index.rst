.. index::
   pair: Index ; Bloom
   ! Bloom index

.. _django_3_1_bloom_index:

==================================================
Django 3.1 new **bloom index** (postgresql)
==================================================

.. seealso::

   - https://docs.djangoproject.com/en/3.1/ref/contrib/postgres/indexes/#bloomindex
