.. index::
   pair: Django 3.1 ; first_name

.. _django_3_1_firstname_150:

=============================================================
**AbstractUser.first_name** max_length increased to **150**
=============================================================

.. seealso::

   - https://docs.djangoproject.com/en/3.1/releases/3.1/#abstractuser-first-name-max-length-increased-to-150




Description
============


A migration for django.contrib.auth.models.User.first_name is included.

If you have a custom user model inheriting from AbstractUser, you’ll need
to generate and apply a database migration for your user model.
