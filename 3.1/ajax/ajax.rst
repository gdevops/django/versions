.. index::
   ! is_ajax

.. _django_3_1_ajax:

============================================================
Django 3.1 **request.is_ajax() is deprecated**
============================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/releases/3.1/#jsonfield-for-all-supported-database-backends





Description
============

**request.is_ajax() is deprecated**

This means we have to recreate the functionality ourselves if we want to
check for an AJAX request. Luckily, the Django developers tell us
exactly what we need to do.

We have to recreate the logic from the request.is_ajax() method ourselves,
which is only 1 line of code:

::

    request.headers.get('x-requested-with') == 'XMLHttpRequest'

Now we can edit the view to include this check


.. code-block:: python

    def ajax_view(request):
      if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        # Get requested data and create data dictionary
        return JsonResponse(data))
