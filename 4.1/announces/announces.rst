
.. _django_4_1_announces:

==========================================================
Announces
==========================================================


Django
=========

- https://x.com/djangoproject/status/1554767355938488321?s=20&t=ul3ltlR8azxA_vCQkS49Pg
- https://www.djangoproject.com/weblog/2022/aug/03/django-41-released/
- https://docs.djangoproject.com/en/4.1/releases/4.1/
- https://docs.djangoproject.com/en/4.1/releases/4.1/#asynchronous-orm-interface
- https://docs.djangoproject.com/en/4.1/releases/4.1/#validation-of-constraints
- https://docs.djangoproject.com/en/4.1/releases/4.1/#form-rendering-accessibility


The Django team is happy to announce the release of Django 4.1.

The release notes cover the profusion of new features in detail, but a few highlights are:

- `An async interface to the ORM <https://docs.djangoproject.com/en/4.1/releases/4.1/#asynchronous-orm-interface>`_, and the ability to define async handlers on class-based views.
- `The use of ORM-defined database constraints <https://docs.djangoproject.com/en/4.1/releases/4.1/#validation-of-constraints>`_ in model validation.
- `Better form rendering accessibility and output style customization <https://docs.djangoproject.com/en/4.1/releases/4.1/#form-rendering-accessibility>`_.

You can get Django 4.1 from our downloads page or from the Python Package Index.
The PGP key ID used for this release is Carlton Gibson: E17DF5C82B4F9D00.

With the release of Django 4.1, Django 4.0 has reached the end of mainstream
support.

The final minor bug fix release, 4.0.7, was issued today. Django 4.0 will
receive security and data loss fixes until April 2023.
All users are encouraged to upgrade before then to continue receiving
fixes for security issues.

See the downloads page for a table of supported versions and the future
release schedule



Django chat podcast
=====================

- https://x.com/ChatDjango/status/1554875869226848263?s=20&t=ul3ltlR8azxA_vCQkS49Pg
- https://djangochat.com/episodes/django-41-preview-jeff-triplett-ep-115-replay

Django 4.1 was released today and to celebrate its release this is a
`replay of an episode from May <https://djangochat.com/episodes/django-41-preview-jeff-triplett-ep-115-replay>`_
with @webology where @carltongibson talks at length about what to expect
in the new major version!


Carlton Gibson
==============

Told you I couldn’t await. 🥁

This is the end point of my post-Django 4.1 “Summer of Async”.

- https://x.com/carltongibson/status/1545032670677188614?s=20&t=ul3ltlR8azxA_vCQkS49Pg
- https://x.com/DjangoConEurope/status/1545027259660115970?s=20&t=ul3ltlR8azxA_vCQkS49Pg

Mariusz Felisiak
====================

- https://x.com/MariuszFelisiak/status/1554769648087965696?s=20&t=ul3ltlR8azxA_vCQkS49Pg

Django 4.1 is out 📣🥳 Many thanks to all contributors 💗and our magnificent
release manager @carltongibson 🎩 #NewReleases #opensource #Python
