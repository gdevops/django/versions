

==========================================================
**Django 4.1 minor versions**
==========================================================

- https://docs.djangoproject.com/en/4.1/releases/4.1/

.. toctree::
   :maxdepth: 3

   4.1.1/4.1.1
