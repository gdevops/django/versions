
.. _django_4_1_1:

==========================================================
Django 4.1.1 (2022-09-05)
==========================================================

- https://www.djangoproject.com/weblog/2022/sep/05/bugfix-release/
- https://docs.djangoproject.com/en/4.1/releases/4.1.1/


Bugfixes
=============

- Reallowed, following a regression in Django 4.1, using GeoIP2()
  when GEOS is not installed (#33886).
- Fixed a regression in Django 4.1 that caused a crash of
  admin’s autocomplete widgets when translations are deactivated (#33888).
- Fixed a regression in Django 4.1 that caused a crash of the test
  management command when running in parallel and multiprocessing start
  method is spawn (#33891).
- Fixed a regression in Django 4.1 that caused an incorrect redirection
  to the admin changelist view when using “Save and continue editing”
  and “Save and add another” options (#33893).
- Fixed a regression in Django 4.1 that caused a crash of
  Window expressions with ArrayAgg (#33898).
- Fixed a regression in Django 4.1 that caused a migration crash on
  SQLite 3.35.5+ when removing an indexed field (#33899).
- Fixed a bug in Django 4.1 that caused a crash of model validation
  on UniqueConstraint() with field names in expressions (#33902).
- Fixed a bug in Django 4.1 that caused an incorrect validation of
  CheckConstraint() with range fields on PostgreSQL (#33905).
- Fixed a regression in Django 4.1 that caused an incorrect migration
  **when adding AutoField, BigAutoField, or SmallAutoField on PostgreSQL (#33919)**.
- Fixed a regression in Django 4.1 that caused a migration crash on
  PostgreSQL when altering AutoField, BigAutoField, or SmallAutoField to OneToOneField (#33932).
- Fixed a migration crash on ManyToManyField fields with through
  referencing models in different apps (#33938).
- Fixed a regression in Django 4.1 that caused an incorrect migration
  when renaming a model with ManyToManyField and db_table (#33953).
- Reallowed, following a regression in Django 4.1, creating reverse
  foreign key managers on unsaved instances (#33952).
- Fixed a regression in Django 4.1 that caused a migration crash on
  SQLite < 3.20 (#33960).
- Fixed a regression in Django 4.1 that caused an admin crash when the
  admindocs app was used (#33955, #33971).
