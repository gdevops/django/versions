.. index::
   pair: Django ; 3.0 ASGI

.. _django_3_0_asgi:

======================================================
**ASGI** (django/core/asgi.py, core/handlers/asgi.py)
======================================================

.. seealso::

   - https://github.com/django/django/tree/master/docs
   - https://github.com/django/django/tree/3.0
   - https://github.com/django/django/blob/master/django/core/asgi.py
   - https://docs.djangoproject.com/en/3.0/releases/3.0/#asgi-support
   - https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
   - https://asgi.readthedocs.io/en/latest/





ASGI support
===============

Django 3.0 begins our journey to making Django fully async-capable by providing
support for running as an ASGI application.

This is in addition to our existing WSGI support.

Django intends to support both for the foreseeable future.

Async features will only be available to applications that run under ASGI, however.

There is no need to switch your applications over unless you want to start
experimenting with asynchronous code, but we have documentation on deploying
with ASGI if you want to learn more.

Note that as a side-effect of this change, Django is now aware of asynchronous
event loops and will block you calling code marked as “async unsafe” - such
as ORM operations - from an asynchronous context.

If you were using Django from async code before, this may trigger if you were
doing it incorrectly.

If you see a SynchronousOnlyOperation error, then closely examine your code
and move any database operations to be in a synchronous child thread.


Articles
==========

.. seealso::

   - https://adamj.eu/tech/2019/09/14/a-single-file-async-django-app/


django/core/asgi.py
=====================

.. seealso::

   - https://github.com/django/django/blob/master/django/core/asgi.py


.. literalinclude:: core_asgi.py
   :linenos:

django/core/handlers/asgi.py
===============================

.. seealso::

   - https://github.com/django/django/blob/master/django/core/handlers/asgi.py


.. literalinclude:: core_handlers_asgi.py
   :linenos:


django/test/signals.py
=========================

.. seealso::

   - https://github.com/django/django/blob/master/django/test/signals.py


.. literalinclude:: test_signals.py
   :linenos:


django/utils/translation/reloader.py
========================================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/translation/reloader.py


.. literalinclude:: utils_translation_reloader.py
   :linenos:


django/utils/translation/trans_real.py
========================================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/translation/trans_real.py


.. literalinclude:: utils_translation_trans_real.py
   :linenos:


django/utils/timezone.py
==========================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/timezone.py


.. literalinclude:: utils_timezone.py
   :linenos:

django/contrib/staticfiles/handlers.py
===========================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/staticfiles/handlers.py


.. literalinclude:: contrib_staticfiles_handlers.py
   :linenos:

tests/asgi/tests.py
======================

.. seealso::

   - https://github.com/django/django/blob/master/tests/asgi/tests.py


.. literalinclude:: tests_asgi_tests.py
   :linenos:

tests/i18n/tests.py
======================

.. seealso::

   - https://github.com/django/django/blob/master/tests/i18n/tests.py


.. literalinclude:: tests_i18n_tests.py
   :linenos:

tests/template_tests/syntax_tests/i18n/test_blocktrans.py
===========================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/template_tests/syntax_tests/i18n/test_blocktrans.py


.. literalinclude:: tests_template_tests_syntax_tests_i18n_test_blocktrans.py
   :linenos:

tests/template_tests/syntax_tests/i18n/test_trans.py
========================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/template_tests/syntax_tests/i18n/test_trans.py


.. literalinclude:: tests_template_tests_syntax_tests_i18n_test_trans.py
   :linenos:

tests/template_tests/syntax_tests/i18n/test_blocktrans.py
===========================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/template_tests/syntax_tests/i18n/test_blocktrans.py


.. literalinclude:: tests_template_tests_syntax_tests_i18n_test_blocktrans.py
   :linenos:
