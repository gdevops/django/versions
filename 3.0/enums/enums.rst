.. index::
   pair: Django 3.0 ; model field choices
   ! Enumeration


.. _enums_model_field_choices:

======================================================================
**Enumerations for model field choices** (django/db/models/enums.py)
======================================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/db/models/enums.py
   - https://docs.djangoproject.com/en/3.0/ref/models/fields/#field-choices-enum-types
   - https://code.djangoproject.com/ticket/27910
   - https://github.com/django/django/blob/master/docs/ref/models/fields.txt
   - https://adamj.eu/tech/2020/01/22/djangos-field-choices-dont-constrain-your-data/
   - https://adamj.eu/tech/2020/01/27/moving-to-django-3-field-choices-enumeration-types/



Description
============

Custom enumeration types TextChoices, IntegerChoices, and Choices are now
available as a way to define Field.choices.

TextChoices and IntegerChoices types are provided for text and integer fields.

The Choices class allows defining a compatible enumeration for other concrete
data types.

These custom enumeration types support human-readable labels that can be
translated and accessed via a property on the enumeration or its members.

See Enumeration types for more details and examples.


Enumeration types
====================

In addition, Django provides enumeration types that you can subclass to define
choices in a concise way:

from django.utils.translation import gettext_lazy as _

.. code-block:: python

    class Student(models.Model):

        class YearInSchool(models.TextChoices):
            FRESHMAN = 'FR', _('Freshman')
            SOPHOMORE = 'SO', _('Sophomore')
            JUNIOR = 'JR', _('Junior')
            SENIOR = 'SR', _('Senior')
            GRADUATE = 'GR', _('Graduate')

        year_in_school = models.CharField(
            max_length=2,
            choices=YearInSchool.choices,
            default=YearInSchool.FRESHMAN,
        )

        def is_upperclass(self):
            return self.year_in_school in {YearInSchool.JUNIOR, YearInSchool.SENIOR}


These work similar to enum from Python's standard library, but with some
modifications:

* Enum member values are a tuple of arguments to use when constructing the
  concrete data type. Django supports adding an extra string value to the end
  of this tuple to be used as the human-readable name, or ``label``. The
  ``label`` can be a lazy translatable string. Thus, in most cases, the member
  value will be a ``(value, label)`` two-tuple. See below for :ref:`an example
  of subclassing choices <field-choices-enum-subclassing>` using a more complex
  data type. If a tuple is not provided, or the last item is not a (lazy)
  string, the ``label`` is :ref:`automatically generated
  <field-choices-enum-auto-label>` from the member name.
* A ``.label`` property is added on values, to return the human-readable name.
* A number of custom properties are added to the enumeration classes --
  ``.choices``, ``.labels``, ``.values``, and ``.names`` -- to make it easier
  to access lists of those separate parts of the enumeration. Use ``.choices``
  as a suitable value to pass to :attr:`~Field.choices` in a field definition.
* The use of :func:`enum.unique()` is enforced to ensure that values cannot be
  defined multiple times. This is unlikely to be expected in choices for a
  field.

Note that using ``YearInSchool.SENIOR``, ``YearInSchool['SENIOR']``, or
``YearInSchool('SR')`` to access or lookup enum members work as expected, as do
the ``.name`` and ``.value`` properties on the members.

.. _field-choices-enum-auto-label:

If you don't need to have the human-readable names translated, you can have
them inferred from the member name (replacing underscores with spaces and using
title-case)::

    >>> class Vehicle(models.TextChoices):
    ...     CAR = 'C'
    ...     TRUCK = 'T'
    ...     JET_SKI = 'J'
    ...
    >>> Vehicle.JET_SKI.label
    'Jet Ski'

Since the case where the enum values need to be integers is extremely common,
Django provides an ``IntegerChoices`` class. For example::

    class Card(models.Model):

        class Suit(models.IntegerChoices):
            DIAMOND = 1
            SPADE = 2
            HEART = 3
            CLUB = 4

        suit = models.IntegerField(choices=Suit.choices)

It is also possible to make use of the `Enum Functional API
<https://docs.python.org/3/library/enum.html#functional-api>`_ with the caveat
that labels are automatically generated as highlighted above::

    >>> MedalType = models.TextChoices('MedalType', 'GOLD SILVER BRONZE')
    >>> MedalType.choices
    [('GOLD', 'Gold'), ('SILVER', 'Silver'), ('BRONZE', 'Bronze')]
    >>> Place = models.IntegerChoices('Place', 'FIRST SECOND THIRD')
    >>> Place.choices
    [(1, 'First'), (2, 'Second'), (3, 'Third')]

.. _field-choices-enum-subclassing:

If you require support for a concrete data type other than ``int`` or ``str``,
you can subclass ``Choices`` and the required concrete data type, e.g.
:class:`~datetime.date` for use with :class:`~django.db.models.DateField`::

    class MoonLandings(datetime.date, models.Choices):
        APOLLO_11 = 1969, 7, 20, 'Apollo 11 (Eagle)'
        APOLLO_12 = 1969, 11, 19, 'Apollo 12 (Intrepid)'
        APOLLO_14 = 1971, 2, 5, 'Apollo 14 (Antares)'
        APOLLO_15 = 1971, 7, 30, 'Apollo 15 (Falcon)'
        APOLLO_16 = 1972, 4, 21, 'Apollo 16 (Orion)'
        APOLLO_17 = 1972, 12, 11, 'Apollo 17 (Challenger)'

There are some additional caveats to be aware of:

- Enumeration types do not support named groups <field-choices-named-groups>.
- Because an enumeration with a concrete data type requires all values to match
  the type, overriding the blank label <field-choices-blank-label>
  cannot be achieved by creating a member with a value of ``None``. Instead,
  set the ``__empty__`` attribute on the class::

    class Answer(models.IntegerChoices):
        NO = 0, _('No')
        YES = 1, _('Yes')

        __empty__ = _('(Unknown)')

.. versionadded:: 3.0

   The ``TextChoices``, ``IntegerChoices``, and ``Choices`` classes were added.


django/db/models/enums.py
============================

.. seealso::

   - https://github.com/django/django/blob/master/django/db/models/enums.py

.. literalinclude:: enums.py
   :linenos:

tests/migrations/test_writer.py
=================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/migrations/test_writer.py


.. literalinclude:: tests_migrations_test_writer.py
   :linenos:


tests/model_enums/tests.py
=============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/model_enums/tests.py



.. literalinclude:: tests_models_enums_tests.py
   :linenos:

tests/model_fields/test_charfield.py
======================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/model_fields/test_charfield.py



.. literalinclude:: tests_model_fields_test_charfield.py
   :linenos:
