.. index::
   pair: Django 3.0 ; PostgreSQL


.. _postgresql_django_3_0:

===============================================================================
PostgreSQL Django 3.0
===============================================================================

.. seealso::

   - :ref:`django_postgresql_version_3_1`
