.. index::
   pair: Django ; 3.0 admin

.. _django_3_0_admin:

============================================================
django.contrib.admin (django/contrib/admin/views/main.py)
============================================================

.. seealso::

   - https://github.com/django/django/tree/master/docs
   - https://github.com/django/django/tree/3.0
   - https://github.com/django/django/tree/master



Description
=============


- Added support for the admin_order_field attribute on properties
  in ModelAdmin.list_display.
- The new ModelAdmin.get_inlines() method allows specifying the inlines based
  on the request or model instance.
- Select2 library is upgraded from version 4.0.3 to 4.0.7.
- jQuery is upgraded from version 3.3.1 to 3.4.1.



admin_order_field
===================

.. toctree::
   :maxdepth: 3

   admin_order_field/admin_order_field
