.. index::
   pair: Django ; 3.0 admin_order_field

.. _django_3_0_admin_order_field:

============================================================
django.contrib.admin admin_order_field
============================================================

.. seealso::

   - https://github.com/django/django/tree/master/docs
   - https://github.com/django/django/tree/3.0
   - https://github.com/django/django/tree/master




Description
=============


- Added support for the admin_order_field attribute on properties
  in ModelAdmin.list_display.

django/contrib/admin/views/main.py
====================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/admin/views/main.py

.. literalinclude:: contrib_admin_views_main.py
   :linenos:


contrib/admin/templatetags/admin_list.py
=========================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/admin/templatetags/admin_list.py

.. literalinclude:: contrib_admin_templatetags_admin_list.py
   :linenos:


tests/admin_views/models.py
============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/models.py


.. literalinclude:: tests_admin_views_models.py
   :linenos:

tests/admin_views/admin.py
==============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/admin.py


.. literalinclude:: tests_admin_views_admin.py
   :linenos:


tests/admin_views/tests.py
=============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/tests.py

.. literalinclude:: tests_admin_views_tests.py
   :linenos:
