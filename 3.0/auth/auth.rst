.. index::
   pair: Django ; 3.0 auth

.. _django_3_0_auth:

=======================================================
django.contrib.auth (django/contrib/auth/backends.py)
=======================================================

.. seealso::

   - https://github.com/django/django/blob/master/docs/ref/contrib/auth.txt
   - https://github.com/django/django/tree/master/django/contrib/auth
   - https://github.com/django/django/blob/master/django/contrib/auth/backends.py
   - https://github.com/django/django/blob/master/tests/auth_tests/test_models.py




Description
==============

- The new **reset_url_token** attribute in PasswordResetConfirmView allows specifying
  a token parameter displayed as a component of password reset URLs.
- Added **BaseBackend class** to ease customization of authentication backends.
- Added **get_user_permissions()** method to mirror the existing
  get_group_permissions() method.
- Added HTML **autocomplete** attribute to widgets of username, email, and password
  fields in django.contrib.auth.forms for better interaction with browser
  password managers.
- **createsuperuser now falls back to environment variables** for password and
  required fields, when a corresponding command line argument isn’t provided
  in non-interactive mode.
- **REQUIRED_FIELDS** now supports ManyToManyFields.
- The new **UserManager.with_perm()** method returns users that have the specified permission.
- The default iteration count for the **PBKDF2** password hasher is increased from
  150,000 to **180,000**.



django/conf/global_settings.py
====================================


.. seealso::

   - https://github.com/django/django/blob/master/django/conf/global_settings.py


.. literalinclude:: django_conf_global_settings.py
   :linenos:




django/contrib/auth/backends.py
==================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/backends.py


.. literalinclude:: backends.py
   :linenos:


django/contrib/auth/base_user.py
====================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/base_user.py


.. literalinclude:: django_contrib_auth_base_user.py
   :linenos:



django/contrib/auth/views.py
===============================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/views.py


.. literalinclude:: django_contrib_auth_views.py
   :linenos:

django/contrib/auth/management/commands/createsuperuser.py
================================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/management/commands/createsuperuser.py


.. literalinclude:: django_contrib_auth_management_commands_createsuperuser.py
   :linenos:



django/contrib/admin/views/autocomplete.py
==============================================

.. seealso::

   - https://github.com/django/django/contrib/admin/views/autocomplete.py

.. literalinclude:: django_contrib_admin_views_autocomplete.py
   :linenos:



django/contrib/auth/forms.py
===============================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/forms.py

.. literalinclude:: django_contrib_auth_forms.py
   :linenos:


django/contrib/admin/checks.py
================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/admin/checks.py

.. literalinclude:: django_contrib_admin_checks.py
   :linenos:



django/contrib/auth/hashers.py
===================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/hashers.py

.. literalinclude:: django_contrib_auth_hashers.py
   :linenos:



django/contrib/admin/options.py
==================================


.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/admin/options.py

.. literalinclude:: django_contrib_admin_options.py
   :linenos:


django/contrib/admin/widgets.py
==================================


.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/admin/widgets.py

.. literalinclude:: django_contrib_admin_widgets.py
   :linenos:




tests/auth_tests/test_models.py
===================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_models.py


.. literalinclude:: auth_tests_test_models.py
   :linenos:


tests/test_client/tests.py
==============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/test_client/tests.py


.. literalinclude:: tests_test_client_tests.py
   :linenos:



tests/auth_tests/test_auth_backends.py
==========================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_auth_backends.py



.. literalinclude:: auth_tests_test_auth_backends.py
   :linenos:


tests/admin_views/urls.py
=============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/urls.py


.. literalinclude:: tests_admin_views_urls.py
   :linenos:


tests/admin_views/admin.py
==============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/admin.py


.. literalinclude:: tests_admin_views_admin.py
   :linenos:


tests/auth_tests/test_views.py
=================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_views.py


.. literalinclude:: tests_auth_tests_test_views.py
   :linenos:



tests/auth_tests/test_forms.py
==================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_forms.py


.. literalinclude:: tests_auth_tests_test_forms.py
   :linenos:


tests/admin_widgets/test_autocomplete_widget.py
===================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_widgets/test_autocomplete_widget.py


.. literalinclude:: tests_admin_widgets_test_autocomplete_widget.py
   :linenos:


tests/modeladmin/test_checks.py
====================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/modeladmin/test_checks.py


.. literalinclude:: tests_modeladmin_test_checks.py
   :linenos:


tests/modeladmin/tests.py
============================

.. seealso::

   - https://github.com/django/django/blob/master/tests/modeladmin/tests.py


.. literalinclude:: tests_modeladmin_tests.py
   :linenos:



tests/admin_views/test_autocomplete_view.py
=============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/admin_views/test_autocomplete_view.py


.. literalinclude:: tests_admin_views_test_autocomplete_view.py
   :linenos:


tests/auth_tests/test_management.py
=====================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_management.py


.. literalinclude:: tests_auth_tests_test_management.py
   :linenos:


tests/auth_tests/test_checks.py
===================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_checks.py


.. literalinclude:: tests_auth_tests_test_checks.py
   :linenos:


tests/auth_tests/models/minimal.py
=======================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/minimal.py


.. literalinclude:: tests_auth_tests_models_minimal.py
   :linenos:

tests/auth_tests/models/custom_permissions.py
================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/custom_permissions.py


.. literalinclude:: tests_auth_tests_models_custom_permissions.py
   :linenos:


tests/auth_tests/models/with_integer_username.py
=====================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/with_integer_username.py


.. literalinclude:: tests_auth_tests_models_with_integer_username.py
   :linenos:


tests/auth_tests/models/with_many_to_many.py
================================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/with_many_to_many.py


.. literalinclude:: tests_auth_tests_models_with_many_to_many.py
   :linenos:


tests/auth_tests/models/custom_user.py
============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/custom_user.py


.. literalinclude:: tests_auth_tests_models_custom_user.py
   :linenos:


tests/auth_tests/models/invalid_models.py
==============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/invalid_models.py


.. literalinclude:: tests_auth_tests_models_invalid_models.py
   :linenos:



tests/auth_tests/models/with_foreign_key.py
==============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/models/with_foreign_key.py


.. literalinclude:: tests_auth_tests_models_with_foreign_key.py
   :linenos:



tests/auth_tests/test_hashers.py
====================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/auth_tests/test_hashers.py


.. literalinclude:: tests_auth_tests_test_hashers.py
   :linenos:


tests/utils_tests/test_crypto.py
===================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/utils_tests/test_crypto.py


.. literalinclude:: tests_utils_tests_test_crypto.py
   :linenos:
