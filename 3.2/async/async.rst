.. index::
   pair: Django 3.2; async

.. _django_3_2_async:

================================================================
Django 3.2 **async ORM** phase 3 is done !
================================================================

.. seealso::

   - https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=75
   - https://forum.djangoproject.com/c/internals/orm/9


.. figure:: phase_3.png
   :align: center

   https://speakerdeck.com/andrewgodwin/just-add-await-retrofitting-async-into-django?slide=75


.. figure:: ../../../versions/3.2/async/phase_3_2.png
   :align: center

   https://2020.pycon.org.au/program/slxvbu/




Taking Django's ORM Async 2020 Fri September 04 by Andrew Godwin
=================================================================

.. seealso::

   - https://2020.pycon.org.au/program/slxvbu/


Django 3.1 has asynchronous views - but the next step is to put asynchronous
support right into the beating heart of Django, its ORM.

Learn the challenges of asynchronous API design in Python, how threading
is crucial even in an async world, and what it takes to teach this ORM
dog new tricks.

The quest to add "hybrid" asynchronous support to Django - where it can
run both synchronous and asynchronous code - is a long one.

Django 3.1 reached an important milestone with synchronous and asynchronous
views, and now the next big step is to take a long, hard look at the
thing that makes up over half the Django codebase: the ORM.

The ORM is gigantic, old, and complex - and has an API designed and
tweaked over many years.

We'll look at some of those design decisions and how they reflect in the
world of async, the challenges that underlie a hybrid API, as well as
how the safety-first nature of the ORM has to evolve to deal with new
and exciting async ways of breaking things.

We'll also dive into what it means to have asynchronous database backends,
and how support for those are progressing in the Python world - and how
we're trying to ship something that's useful before fully asynchronous
database APIs are done.

Twitter threads
==================

.. seealso::

   - https://x.com/chriswithers13/status/1277988457416208384?s=20
   - https://x.com/andrewgodwin/status/1278500115494260737?s=20
   - https://x.com/andrewgodwin/status/1278518570796769282?s=20
