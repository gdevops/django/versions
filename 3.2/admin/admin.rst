
.. _django_3_2_admin:

==================================================
Django 3.2 admin
==================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/releases/3.2
   - https://github.com/django/django/blob/master/docs/releases/3.2.txt

- ModelAdmin.search_fields` now allows searching against quoted phrases
  with spaces.
