.. index::
   pair: Expressions ; Django 3.2

.. _django_3_2_expressions:

================================================================
**Django 3.2 LTS**  Expressions can reference transforms
================================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/db/queries/#using-transforms-in-expressions
   - https://github.com/django/django/commit/83fcfc9ec8610540948815e127101f1206562ead





Expressions can reference transforms
=======================================

New in Django 3.2.

Django supports using transforms in expressions.

For example, to find all Entry objects published in the same year as they
were last modified::

    >>> Entry.objects.filter(pub_date__year=F('mod_date__year'))

To find the earliest year an entry was published, we can issue the query::

    >>> Entry.objects.aggregate(first_published_year=Min('pub_date__year'))

This example finds the value of the highest rated entry and the total
number of comments on all entries for each year::

    >>> Entry.objects.values('pub_date__year').annotate(
    ...     top_rating=Subquery(
    ...         Entry.objects.filter(
    ...             pub_date__year=OuterRef('pub_date__year'),
    ...         ).order_by('-rating').values('rating')[:1]
    ...     ),
    ...     total_comments=Sum('number_of_comments'),
    ... )
