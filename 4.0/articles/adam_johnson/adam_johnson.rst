
.. _django_4_0_adam_johnson:

==========================================================
**Django 4.0** Adam Johnson
==========================================================

- https://adamj.eu/tech/2021/12/08/pre-order-boost-your-django-dx/

New Testing Features in Django 4.0
======================================


- https://adamj.eu/tech/2021/09/28/new-testing-features-in-django-4.0/


Django 4.0’s five headline features
=========================================

- https://x.com/AdamChainz/status/1468529890051952686?s=20


#1 zoneinfo as the default timezone implementation
-------------------------------------------------------

- https://x.com/AdamChainz/status/1468529898436370432?s=20

Django historically relied on pytz for timezones, but it has... issues.

https://x.com/pganssle contributed zoneinfo to Python's standard
library, to address these. Django now uses zoneinfo.

Thanks to https://x.com/carltongibson for contributing this.

now::

    In [6]: import zoneinfo
    In [7]: zoneinfo.ZoneInfo("Europe/Paris")
    Out[7]: zoneinfo.ZoneInfo(key='Europe/Paris')

Before::

    In [8]: import pytz
    In [9]: pytz.timezone("Europe/Paris")
    Out[9]: <DstTzInfo 'Europe/Paris' LMT+0:09:00 STD>


#2 Functional unique constraints
-----------------------------------

- https://github.com/django/django/pull/11929 (Added support for functional indexes)
- https://github.com/django/django/pull/13983 (Added support for functional unique constraints)
- https://github.com/django/django/commit/83fcfc9ec8610540948815e127101f1206562ead
- https://x.com/AdamChainz/status/1468529903276593158?s=20

You can now use database functions to define unique constraints.
This allows you to enforce uniqueness on transformed values, e.g. to
ensure email addresses unique despite use of upper/lower case

Thanks to https://x.com/hannseman for contributing this.

.. code-block:: python

    class Person(models.Model):
    ...

    class Meta:
        constraints = [
            models.UniqueConstraint(
               Lower("email"),
               name ="%(app_label)s_%(class)s_unique_email",
            )
        ]

#3 scrypt password hasher
------------------------------

- https://x.com/AdamChainz/status/1468529910046146563?s=20
- https://en.wikipedia.org/wiki/Cryptographic_hash_function

Django defaults to using `PBKDF2 <https://en.wikipedia.org/wiki/PBKDF2>`_ (https://en.wikipedia.org/wiki/PBKDF2) for password hashing.

`Scrypt <https://en.wikipedia.org/wiki/Scrypt>`_ (https://en.wikipedia.org/wiki/Scrypt) is
a stronger choice that requires more memory, to slow down attacks.

**Django now includes a scrypt hasher**.

Thanks to :ref`Anthony Wright <https://x.com/Ryo>`) for contributing this

.. code-block:: python

    PASSWORD_HASHERS = [
        "django.contrib.auth.hashers.ScrypPasswordHasher",
        ...,
    ]

#4 Built-in Redis cache backend
--------------------------------------

- https://github.com/django/django/pull/14437 (Added Redis cache backend)
- https://docs.djangoproject.com/en/dev/topics/cache/#redis
- https://x.com/AdamChainz/status/1468529915347849218?s=20
- https://x.com/carltongibson/status/1437786664760397835?s=20

https://x.com/Redisinc is very popular with Django developers.
Previously it was mainly used via the django-redis package, but now
it’s built-in!

Thanks to https://x.com/Abbasidaniyal\_ for contributing this as a
Google of Summer of Code project.


.. code-block:: python

    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.redis.RedisCache",
            "LOCATION": "redis://127.0.0.1:6379",
        }
    }

Big congratulations to https://x.com/Abbasidaniyal/Abbasidaniyal\_
for his successful GSoC project adding a Redis cache backend to Django.
Coming to you in Django 4.0. 🎁🎉

.. figure:: images/carlton.png
   :align: center

   https://x.com/carltongibson/status/1437786664760397835?s=20

.. figure:: images/abbasi.png
   :align: center

   https://x.com/Abbasidaniyal\_/status/1437802277251661829?s=20


#5 Template-based form rendering
--------------------------------------

- https://docs.djangoproject.com/en/dev/releases/4.0/#template-based-form-rendering
- https://docs.djangoproject.com/en/dev/ref/forms/api/#django.forms.Form.render
- https://docs.djangoproject.com/fr/4.0/ref/forms/api/#render
- https://x.com/AdamChainz/status/1468529922830438406?s=20

Django already rendered changed to render form widgets through templates.

Now the remaining pieces of the form process go through templates,
which you can replace with custom display logic.

Thanks to https://x.com/David_Smith86 and https://x.com/JohannesMaron
.
