.. index::
   pair: Django 4.0 ; Security



.. _django_4_0_security:

==========================================================
CSRF_TRUSTED_ORIGINS changes (Cross-site request forgery)
==========================================================

- https://en.wikipedia.org/wiki/Cross-site_request_forgery
- https://docs.djangoproject.com/en/dev/releases/4.0/#csrf-trusted-origins-changes
- https://docs.djangoproject.com/en/dev/ref/csrf/#how-it-works

Documentation
==============

- :ref:`tuto_secops:csrf`

Format change
====================

Values in the CSRF_TRUSTED_ORIGINS setting must include the scheme
(e.g. 'http://' or 'https://') instead of only the hostname.

Also, values that started with a dot, must now also include an asterisk
before the dot. For example, change '.example.com' to 'https://\*.example.com'.

A system check detects any required changes.


Configuring it may now be required
-------------------------------------

As CSRF protection now consults the Origin header, you may need to set
CSRF_TRUSTED_ORIGINS, particularly if you allow requests from subdomains
by setting CSRF_COOKIE_DOMAIN (or SESSION_COOKIE_DOMAIN if CSRF_USE_SESSIONS
is enabled) to a value starting with a dot.


Example::

    ALLOWED_HOSTS = [
        "localhost",
        "0.0.0.0",
        "intranet.srv.xxx.eu",
        "intranet-staging.srv.xxx.eu",
    ]
    # https://groups.google.com/g/django-developers/c/W_RiCsguaSU/?pli=1
    CSRF_TRUSTED_ORIGINS = [
        "http://0.0.0.0",
        "http://localhost",
        "https://intranet.srv.xxx.eu",
        "https://intranet-staging.srv.xxx.eu",
    ]
