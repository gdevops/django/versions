
.. _migrations_autodetector:

==========================================================
**Migrations autodetector**
==========================================================

- https://docs.djangoproject.com/en/dev/releases/4.0/#migrations-autodetector-changes


The migrations autodetector now uses **model states** instead of **model classes**.

Also, migration operations for ForeignKey and ManyToManyField fields no
longer specify attributes which were not passed to the fields during
initialization.

As a side-effect, running makemigrations might generate no-op AlterField
operations for ManyToManyField and ForeignKey fields in some cases.
