.. index::
   pair: Django 4.0 ; Forms

.. _django_4_0_forms:

==========================================================
Forms in Django 4.0+
==========================================================



Forms in Django 4.0+
======================

- https://smithdc1.github.io/my-blog/2022/forms/forms.html


Background
--------------

Prior to Django 4.0 forms were rendered by string concatenation.

A number of 3rd party packages allowed for forms to be rendered as
templates e.g. floppy forms, crispy-forms.

Django 4.0 introduced the capability to render forms using the template
engine. This allowed the form template to be set per form class or per
instance if a template name is provided when calling render().
